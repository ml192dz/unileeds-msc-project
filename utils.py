import torch
from vocabulary import Vocabulary
from string import digits
from collections import Counter
from config import *

def read_lines(filepath):
    """ 
    Function: 
        Open the ground truth captions into memory, line by line. 
    Args:
        filepath (str): the complete path to the tokens txt file
    """
    file = open(filepath, 'rb')
    lines = []

    while True: 
        # Get next line
        line = file.readline() 
        if not line: 
            break
        lines.append(line.strip())
    file.close()
    return lines


def parse_lines(lines):
    """
    Function:
        Parses token file captions into image_ids and captions.
    Args:
        lines (str list): str lines from token file
    Return:
        image_ids (int list): list of image ids, with duplicates
        cleaned_captions (list of lists of str): lists of words
    """
    image_ids = []
    cleaned_captions = []
     
    for _str in lines:
        cont = _str.decode().split('\t')
        symbols = '"!?.-#&():'
        image_ids.append(cont[0].split('.')[0])
        cleaned_caption = cont[1]
        for s in symbols:
            cleaned_caption = cleaned_caption.replace(s, '')
        #del number and rstrip so on
        cleaned_caption = cleaned_caption.replace(', ', '')
        cleaned_caption = cleaned_caption.replace(',', '')
        cleaned_caption = cleaned_caption.replace('; ', '')
        cleaned_caption = cleaned_caption.replace("'", '')
        cleaned_caption = cleaned_caption.lower().rstrip().lstrip().translate(str.maketrans('', '', digits))
        cleaned_captions.append(cleaned_caption)

    return image_ids, cleaned_captions


def build_vocab(cleaned_captions):
    """ 
    Function:
        Parses training set token file captions and builds a Vocabulary object
    Args:
        cleaned_captions (str list): cleaned list of human captions to build vocab with

    Returns:
        vocab (Vocabulary): Vocabulary object
    """

    # Collect words
    collect_words = []
    temp_words = []
    for _str1 in cleaned_captions:
        _str2 = _str1.split(" ")
        for _str3 in _str2:
            if (_str3 != ""):
                collect_words.append(_str3)

    result = Counter(collect_words)
    for key, value in result.items():
        if (value > MIN_FREQUENCY): #Throw out words that appear less than three times
            temp_words.append(key)

    temp_words = list(set(temp_words))
    temp_words.sort()

    # create a vocab instance
    vocab = Vocabulary()
    # add the token words
    vocab.add_word('<pad>')
    vocab.add_word('<start>')
    vocab.add_word('<end>')
    vocab.add_word('<unk>')
    # add the rest of the words from the cleaned captions 
    for word in temp_words:
        vocab.add_word(word)
    return vocab



def decode_caption(sampled_ids, vocab):
    """ 
    Function:
        according to the sampled_ids , to get the prediction caption
    Args:
        sampled_ids (int list): list of word IDs from decoder
        vocab (Vocabulary): vocab for conversion
    Return:
        predicted_caption (str): predicted string sentence
    """
    predicted_caption = ""

    sampled_caption = []
    for word_id in sampled_ids:
        word = vocab.idx2word[word_id]
        sampled_caption.append(word)
        if word == '<end>':
            break
    predicted_caption = ' '.join(sampled_caption)

    if sampled_caption[0] == "<start>":
        sampled_caption.remove("<start>")

    if sampled_caption[-1] == "<end>":
        sampled_caption.remove("<end>")

    return predicted_caption,sampled_caption



def caption_collate_fn(data):
    """ 
    Function:
        Creates mini-batch tensors from the list of tuples (image, caption).
    Args:
        data: list of tuple (image, caption). 
            - image: torch tensor of shape (3, 224, 224).
            - caption: torch tensor; variable length.
    Returns:
        images: torch tensor of shape (batch_size, 3, 224, 224).
        targets: torch tensor of shape (batch_size, padded_length).
        lengths: list; valid length for each padded caption.
    """
    # Sort a data list by caption length from longest to shortest.
    data.sort(key=lambda x: len(x[1]), reverse=True)
    images, captions = zip(*data)

    # merge images (from tuple of 3D tensor to 4D tensor).
    # if using features, 2D tensor to 3D tensor. (batch_size, 256)
    images = torch.stack(images, 0) 

    # merge captions (from tuple of 1D tensor to 2D tensor).
    lengths = [len(cap) for cap in captions]
    targets = torch.zeros(len(captions), max(lengths)).long()
    for i, cap in enumerate(captions):
        end = lengths[i]
        targets[i, :end] = cap[:end]

    return images, targets, lengths


