'''
Constant initialization
'''

# path to images(could replace Flicker30k to Flicker8k)
IMAGE_DIR = "Flicker30k_Dataset/" 

IMAGE_DIR = "Flicker30k_Dataset/Flicker30k_Images/"
# path to caption data
CAPTION_DIR = ""
# token file names to read fromTOKEN_FILE_TEST
TOKEN_FILE_TRAIN = CAPTION_DIR + "Flicker30k_Dataset/Flicker30k_train.token.txt"
TOKEN_FILE_TEST = CAPTION_DIR + "Flicker30k_Dataset/Flicker30k_test.token.txt"


EMBED_SIZE = 256
HIDDEN_SIZE = 512
NUM_LAYERS = 1
LR = 0.001
NUM_EPOCHS = 5
LOG_STEP = 20
MIN_FREQUENCY = 3

